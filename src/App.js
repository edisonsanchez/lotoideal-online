import React from 'react'

import { Router } from '@reach/router'

export const App = () => {
  return (
      <Router>                                //  PATHS
        <NotFound default />                  //  NO PAGE
        <Home path="/" />                      //  "/"
      </Router>
  )
}
