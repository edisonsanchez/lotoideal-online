import React, { createContext, useState } from 'react'
export const Context = createContext()

const Provider = ({ children }) => {
  const [isAuth, setIsAuth] = useState(() => {

    console.log('isAuth: ' + isAuth);
    console.log('token: ' + window.sessionStorage.getItem('token'));
    return window.sessionStorage.getItem('token')
  })

  const value = {
    isAuth,
    activateAuth: token => {
      setIsAuth(true)

      console.log('setIsAuth(true): ' + token);

      window.sessionStorage.setItem('token', token)
    },
    removeAuth: () => {
      setIsAuth(false)

      console.log('setIsAuth(false): ');

      window.sessionStorage.removeItem('token')
    }
  }

  return (
    <Context.Provider value={value}>
      {children}
    </Context.Provider>
  )
}

export default {
  Provider,
  Consumer: Context.Consumer
}
