import React, { createContext, useState, useEffect } from 'react'
import Amplify from 'aws-amplify'
import config from '../aws-exports'
import { Hub, Auth } from 'aws-amplify'

Amplify.configure(config)
export const Context = createContext()

const AUTHENTICATOR_AUTHSTATE = 'amplify-authenticator-authState'

const AuthProvider = props => {
//const Provider = ({ children }) => {
  const [isAuth, setIsAuth] = useState(() => {

    console.log('isAuth: ' + isAuth);
    console.log('token: ' + window.sessionStorage.getItem('token'));
    return window.sessionStorage.getItem(AUTHENTICATOR_AUTHSTATE)
  })

  useEffect(() => {
    Auth.currentAuthenticatedUser()
      .then(user => {
        console.log('Current user: ', user)
        localStorage.setItem(AUTHENTICATOR_AUTHSTATE, 'signedIn')
        setAuth({ state: 'signIn', user })
      })
      .catch(err => localStorage.getItem(AUTHENTICATOR_AUTHSTATE))
      .then(cachedAuthState => cachedAuthState === 'signedIn' && Auth.signOut())
  }, [])
  useEffect(() => {
  Hub.listen('auth', ({ payload }) => {
  const { event, data } = payload
  if (event === 'signOut') {
  setAuth({ state: event, user: null })
  localStorage.deleteItem(AUTHENTICATOR_AUTHSTATE)
  return
  }
  localStorage.setItem(AUTHENTICATOR_AUTHSTATE, 'signedIn')
  setAuth({ state: event, user: data })
  })
  }, [])

  return <Provider value={isAuth}>{props.children}</Provider>

}


export default {
  AuthProvider,
  AuthConsumer: Context.Consumer
}

