import React from 'react'
import { Layout } from '../../components/Layout'

const MatchPage = (props) => {
  return (
    <Layout title='Match' subtitle='Subtitulo'>
      <p>Match Page</p>
    </Layout>
  )
}

export const Match = React.memo(MatchPage)
