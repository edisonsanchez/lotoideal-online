import React from 'react'
import { Layout } from '../../components/Layout'

const PlayPage = (props) => {
  return (
    <Layout title='Play' subtitle='Subtitulo'>
      <p>Play Page</p>
    </Layout>
  )
}

export const Play = React.memo(PlayPage)
