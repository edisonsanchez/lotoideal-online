import React from 'react'
import { Layout } from '../../components/Layout'

const StorePage = (props) => {
  return (
    <Layout title='Store' subtitle='Subtitulo'>
      <p>Store Page</p>
    </Layout>
  )
}

export const Store = React.memo(StorePage)
