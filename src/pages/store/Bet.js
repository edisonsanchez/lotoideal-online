import React from 'react'
import { Layout } from '../../components/Layout'

const BetPage = (props) => {
  return (
    <Layout title='Bet' subtitle='Subtitulo'>
      <p>Bet Page</p>
    </Layout>
  )
}

export const Bet = React.memo(BetPage)
