import React from 'react'
import { Layout } from '../../components/Layout'

const BillingPage = (props) => {
  return (
    <Layout title='Billing' subtitle='Subtitulo'>
      <p>Billing Page</p>
    </Layout>
  )
}

export const Billing = React.memo(BillingPage)
