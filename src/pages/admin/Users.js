import React from 'react'
import { Layout } from '../../components/Layout'

const UsersPage = (props) => {
  return (
    <Layout title='Users' subtitle='Subtitulo'>
      <p>Users Page</p>
    </Layout>
  )
}

export const Users = React.memo(UsersPage)
