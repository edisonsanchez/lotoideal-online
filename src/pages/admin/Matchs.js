import React from 'react'
import { Layout } from '../../components/Layout'

const MatchsPage = (props) => {
  return (
    <Layout title='Matchs' subtitle='Subtitulo'>
      <p>Matchs Page</p>
    </Layout>
  )
}

export const Matchs = React.memo(MatchsPage)
