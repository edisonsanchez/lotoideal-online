import React from 'react'
import { Layout } from '../../components/Layout'

const ReportPage = (props) => {
  return (
    <Layout title='Report' subtitle='Subtitulo'>
      <p>Report Page</p>
    </Layout>
  )
}

export const Report = React.memo(ReportPage)
