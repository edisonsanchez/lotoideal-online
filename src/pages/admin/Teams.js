import React from 'react'
import { Layout } from '../../components/Layout'

const TeamsPage = (props) => {
  return (
    <Layout title='Teams' subtitle='Subtitulo'>
      <p>Teams Page</p>
    </Layout>
  )
}

export const Teams = React.memo(TeamsPage)
