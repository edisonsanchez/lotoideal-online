import React from 'react'
import { Layout } from '../../components/Layout'

const DashboardPage = (props) => {
  return (
    <Layout title='Dashboard' subtitle='Subtitulo'>
      <p>Dashboard Page</p>
    </Layout>
  )
}

export const Dashboard = React.memo(DashboardPage)
