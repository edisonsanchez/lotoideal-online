import React from 'react'
import { Layout } from '../../components/Layout'

const SportsPage = (props) => {
  return (
    <Layout title='Sports' subtitle='Subtitulo'>
      <p>Sports Page</p>
    </Layout>
  )
}

export const Sports = React.memo(SportsPage)
