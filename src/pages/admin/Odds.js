import React from 'react'
import { Layout } from '../../components/Layout'

const OddsPage = (props) => {
  return (
    <Layout title='Odds' subtitle='Subtitulo'>
      <p>Odds Page</p>
    </Layout>
  )
}

export const Odds = React.memo(OddsPage)
