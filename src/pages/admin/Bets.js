import React from 'react'
import { Layout } from '../../components/Layout'

const BetsPage = (props) => {
  return (
    <Layout title='Bets' subtitle='Subtitulo'>
      <p>Bets Page</p>
    </Layout>
  )
}

export const Bets = React.memo(BetsPage)
