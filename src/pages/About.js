import React from 'react'
import { Layout } from '../components/Layout'

const AboutPage = (props) => {
  return (
    <Layout title='About' subtitle='Subtitulo'>
      <p>About Page</p>
    </Layout>
  )
}

export const About = React.memo(AboutPage)
