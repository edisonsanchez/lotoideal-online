import React from 'react'
import { Layout } from '../components/Layout'

const SupportPage = (props) => {
  return (
    <Layout title='Support' subtitle='Subtitulo'>
      <p>Support Page</p>
    </Layout>
  )
}

export const Support = React.memo(SupportPage)
