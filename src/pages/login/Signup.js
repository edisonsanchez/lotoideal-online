import React from 'react'
import { Layout } from '../../components/Layout'
import { FormLogin } from '../../components/forms'
import { useUser } from '../../contexts/Context';


const SignupPage = () => {
  const { login, social } = useUser();

  return (
    <Layout title='LOTO IDEAL SPORTS | Registrar'>
      <FormLogin disabled={false} error={null} title='Registrar' onSubmit={login} onSocial={social}/>
    </Layout>
  )
}

export const Signup = React.memo(SignupPage)
