import React from 'react'
import { Layout } from '../../components/Layout'
import { FormLogin } from '../../components/forms'
// import { useUser } from '../../contexts/Context';

// Handle Submit
// const handleSubmit = async (event) => {
//   event.preventDefault();

//   try {
//     await Auth.signIn(email, password);
//     alert("Logged in");
//   } catch (e) {
//     alert(e.message);
//   }
// }

// Login Page
const LoginPage = () => {
  // const { login, social } = useUser();

  return (
    <Layout title='LOTO IDEAL SPORTS | Login'>
      <FormLogin disabled={false} error={null} title='Iniciar sesión' onSubmit={login} onSocial={social}/>
    </Layout>
  )
}

export const Login = React.memo(LoginPage)
