import React from 'react'
import { Layout } from '../../components/Layout'
import { FormLogin } from '../../components/forms'
import { useUser } from '../../contexts/Context';


const ConfirmPage = () => {
  const { login, social } = useUser();

  return (
    <Layout title='LOTO IDEAL SPORTS | Confirm'>
      <FormLogin disabled={false} error={null} title='Confirm' onSubmit={login} onSocial={social}/>
    </Layout>
  )
}

export const Confirm = React.memo(ConfirmPage)
