import React from 'react'
import { Layout } from '../../components/Layout'
import { FormLogin } from '../../components/forms'
import { useUser } from '../../contexts/Context';


const LogoutPage = () => {
  const { login, social } = useUser();

  return (
    <Layout title='LOTO IDEAL SPORTS | Login'>
      <FormLogin disabled={false} error={null} title='Iniciar sesión' onSubmit={login} onSocial={social}/>
    </Layout>
  )
}

export const Logout = React.memo(LogoutPage)
