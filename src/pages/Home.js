import React from 'react'
import { Layout } from '../components/Layout'

const HomePage = (props) => {
  return (
    <Layout title='Home' subtitle='Subtitulo'>
      <p>HOME HOME SWEET HOME</p>
    </Layout>
  )
}

const Home = React.memo(HomePage)

export default Home
