import React from 'react'
import { GlobalStyle } from './styles/GlobalStyles'
import { NavBar } from './components/NavBar'

// Routes
import { Home } from './pages/Home'
import { About } from './pages/About'
import { Support } from './pages/Support'
// Login
import { Login } from './pages/login/Login'
import { Signup } from './pages/login/Signup'
import { Confirm } from './pages/login/Confirm'
// Store
import { Store } from './pages/store/Store'
import { Match } from './pages/store/Match'
import { Play } from './pages/store/Play'
import { Bet } from './pages/store/Bet'
import { Billing } from './pages/store/Billing'
// Store
import { Dashboard } from './pages/admin/Dashboard'
import { Report } from './pages/admin/Report'
import { Sports } from './pages/admin/Sports'
import { Matchs } from './pages/admin/Matchs'
import { Teams } from './pages/admin/Teams'
import { Bets } from './pages/admin/Bets'
import { Users } from './pages/admin/Users'
import { Odds } from './pages/admin/Odds'

import { NotFound } from './pages/NotFound'

import { Router, Redirect } from '@reach/router'
import { useUser } from './contexts/Context'

//const Favs = React.lazy(() => import('./pages/Favs'))

export const App = () => {
  // const { user } = useUser();

  // console.log("App, user: " + JSON.stringify(user));


  // const isAuth = user? true : false

  // console.log("App, isAuth: " + (isAuth));


  return (
    <React.Suspense fallback={<div />}>
      <GlobalStyle />
      <Router>                                //  PATHS
        <NotFound default />                  //  NO PAGE
        <Home path="/"/>                      //  "/"
        <About path="about"/>                 //  "/about"
        <Support path="support"/>             //  "/support"
        <Login path="signin">                 //  "/login"
          <Signup path="signup"/>             //  "/login/signup"
          <Confirm path="confirm"/>           //  "/login/confirm"
        </Login>
        <Store path="store">                  //  "/store"
          <Match path="match"/>               //  "/store/match"
          <Match path="match/:matchId"/>      //  "/store/match/:matchId"
          <Play path="play"/>                 //  "/store/play"
          <Bet path="bet"/>                   //  "/store/bet"
          <Bet path="bet/:betId"/>            //  "/store/bet/:betId"
          <Billing path="billing"/>           //  "/store/billing"
        </Store>
        <Dashboard path="dashboard">          //  "/dashboard"
          <Report path=":reportId"/>          //  "/dashboard/:reportId"
          <Sports path="sports"/>             //  "/dashboard/sports"
          <Teams path="teams"/>               //  "/dashboard/teams"
          <Matchs path="matchs"/>             //  "/dashboard/matchs"
          <Bets path="games"/>                //  "/dashboard/games"
          <Users path="reports"/>             //  "/dashboard/users"
          <Odds path="team"/>                 //  "/dashboard/odds"
        </Dashboard>
      </Router>
      <NavBar />
    </React.Suspense>
  )
}
