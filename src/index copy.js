import React from 'react'
import ReactDOM from 'react-dom'
// import ApolloClient from 'apollo-boost'
// import { ApolloProvider } from 'react-apollo'

// import { UserProvider } from './contexts/Context'

import { App } from './App'

// const client = new ApolloClient({
//   uri: 'https://petgram-server.midudev.now.sh/graphql',
//   request: operation => {
//     const token = window.sessionStorage.getItem('token')
//     const authorization = token ? `Bearer ${token}` : ''
//     operation.setContext({
//       headers: {
//         authorization
//       }
//     })
//   },
//   onError: error => {
//     const { networkError } = error
//     if (networkError && networkError.result.code === 'invalid_token') {
//       window.sessionStorage.removeItem('token')
//       window.location.href = '/'
//     }
//   }
// })

// ReactDOM.render(
//   // <UserProvider>
//     {/* <ApolloProvider client={client}> */}
//       <App />
//     {/* </ApolloProvider> */}
//   // </UserProvider>
//   ,document.getElementById('app'))
ReactDOM.render(<App />, document.getElementById("app"));
