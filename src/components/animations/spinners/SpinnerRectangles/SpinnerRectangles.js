import React from'react'
import { SquareLoading, SquareInside } from'./styles'

//Three Rectangles Loop.
export const SpinnerRectangles = () => {
  return (
    <SquareLoading>
      <SquareInside />
      <SquareInside />
      <SquareInside />
    </SquareLoading>
  )
}
