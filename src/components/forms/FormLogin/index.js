import React, { Fragment } from 'react'
import { useInputValue } from '../../../hooks/useInputValue'
import { Error, Form, Input, Title, Wrapper } from './styles'
import { SubmitButton } from '../../SubmitButton'
import { ButtonSocial } from '../../buttons/ButtonSocial'

export default ({ error, disabled, onSubmit, onSocial, title }) => {
  const email = useInputValue('')
  const password = useInputValue('')

  const handleSubmit = (event) => {
    event.preventDefault()

    console.log('email.value: ' + email.value)
    console.log('password.value: ' + password.value)
    onSubmit(email.value, password.value)
  }

  const handleFacebook = (event) => {
    event.preventDefault()

    console.log('handleFacebook ')

    onSocial('Facebook')
  }

  const handleGoogle = (event) => {
    event.preventDefault()

    console.log('handleGoogle ')

    onSocial('Google')
  }

  return (
    <Fragment>
      <Form disabled={disabled} onSubmit={handleSubmit}>
        <Title>{title}</Title>
        <Input disabled={disabled} placeholder='Email' {...email} />
        <Input disabled={disabled} placeholder='Password' type='password' {...password} />
        <SubmitButton disabled={disabled}>{title}</SubmitButton>
      </Form>
      { social &&
        <React.Fragment>
          <Wrapper>
            <ButtonSocial disabled={disabled} onClick={handleFacebook}>{'Ingresar con Facebook'}</ButtonSocial>
            <ButtonSocial disabled={disabled} onClick={handleGoogle} google>{'Ingresar con Google'}</ButtonSocial>
          </Wrapper>
          <SubmitButton disabled={disabled}>Registrarse</SubmitButton>
        </React.Fragment>

      }
      {error && <Error>{error}</Error>}
    </Fragment>
  )
}
