import React from 'react'
import { Anchor, Image } from './styles'

const DEFAULT_IMAGE = 'https://i.imgur.com/dJa0Hpl.jpg'

export const BtnImageRounded = ({ cover = DEFAULT_IMAGE, path }) => (
  <Anchor href={path}>
    <Image src={cover} />
  </Anchor>
)
