import React from 'react'
import { Wrapper, ButtonFacebook, ButtonGoogle } from './styles'
import PropTypes from 'prop-types'

export const ButtonSocial = ({ children, disabled, onClick, google }) => {
  
  return google? <ButtonGoogle disabled={disabled} onClick={onClick}>{children}</ButtonGoogle> :
  <ButtonFacebook disabled={disabled} onClick={onClick}>{children}</ButtonFacebook>
  
}

// ButtonSocial.propTypes = {
//   disabled: PropTypes.bool,
//   onClick: PropTypes.func,
//   children: PropTypes.node.isRequired
// }
