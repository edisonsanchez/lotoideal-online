
describe('Tests', function() {
    it("It's available?", function() {
        cy.visit('/')
    })

    it("Route?", function() {
        cy.visit('/pet/1')
        cy.get('article')
    })

    it("NavBar Working?", function() {
        cy.visit('/pet/1')
        cy.get('nav a').first().click()
        cy.url().should('include ', '/')
    })
})